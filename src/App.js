import AppNavbar from './components/AppNavbar';
import Home from './pages/Home'
import './App.css';

import {Container} from 'react-bootstrap';

function App() {
  return (

  <fragment>
    <AppNavbar />
      <Container>
        <Home />
      </Container>
  </fragment>

  );
}

export default App;
